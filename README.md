# TES3MP

*Note: This repository is experimental and not particularly functional at the moment. Contributions are always appreciated.*

This is the [Portmod](https://gitlab.com/portmod/portmod) package repository for server-side [TES3MP](https://github.com/TES3MP/openmw-tes3mp).

See the Portmod [Guide](https://gitlab.com/portmod/portmod/-/wikis/home#guide) for details on how to get started.

## Usage

TES3MP server installs and manages the CoreScripts directory, as well as custom scripts. TES3MP will need to be pointed towards the prefix ROOT (see `portmod <prefix> info` for the precise location).
